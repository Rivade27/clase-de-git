***Settings***
Library    String
Library    SeleniumLibrary

***Variables***
${Browser}    chrome
${Homepage}    automationpractice.com/index.php
${scheme}    http 
${TestUrl}        ${scheme}://${Homepage}

#http://automationpractice.com/index.php
#${ProdScheme}    https ESQUEMA DE PRODUCCION
#${prodURL}    ${ProdScheme}://${Homepage} ESQUEMA DE PRODUCCION

***Keywords***
Open Homepage
    Open Browser        ${TestUrl}   ${Browser}

***Test Cases***
TC001 Hacer click en contenedores
    Open Homepage
    Set Global Variable     @{nombresDeContenedores}    //*[@id="homefeatured"]/li[1]/div/div[2]/h5/a     //*[@id="homefeatured"]/li[2]/div/div[2]/h5/a     //*[@id="homefeatured"]/li[3]/div/div[2]/h5/a     //*[@id="homefeatured"]/li[4]/div/div[2]/h5/a     //*[@id="homefeatured"]/li[5]/div/div[2]/h5/a     //*[@id="homefeatured"]/li[6]/div/div[2]/h5/a      //*[@id="homefeatured"]/li[7]/div/div[2]/h5/a
    FOR     ${NombreDeContenedor}     IN     @{nombresDeContenedores}
        Click Element    xpath=${NombreDeContenedor}
        Wait Until Element Is Visible     xpath=//*[@id="bigpic"]
        Click Element   xpath=//*[@id="header_logo"]/a/img  
    END
    Close Browser

TC002 Caso de prueba nuevo para git
    Open Homepage
    